'use strict';
angular.module('myApp.customersService', [])
    .factory('customersService', function ($http, $log, $q) {

        var customer = {};
        var credit = {};
        var table = {};

        // Servicio que regresa la lista de los clientes
        function getCustomers() {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'http://localhost:3000/customers'
            }).then(function (data) {
                deferred.resolve(data.data);
            }, function (error) {
                alert("error");
                deferred.reject(error);
            });
            return deferred.promise;
        }

        // Servicio que regresa los creditos
        function getCredits(idCustomer) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'http://localhost:3000/credits'
            }).then(function (data) {
                /* Se obtienen todos los resultado y se filtra unicamente el del cliente seleccionado,
                se realiza de esta forma ya que no se cuenta con un backend */
                var ownCredits = data.data.filter(data => data.idCustomer == idCustomer)
                deferred.resolve(ownCredits);
            }, function (error) {
                alert("error");
                deferred.reject(error);
            });
            return deferred.promise;
        }

        // Servicio que regresa las tablas de amortización
        function getTable(idCredit) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: 'http://localhost:3000/tables'
            }).then(function (data) {
                /* Se obtienen todos los resultado y se filtra unicamente el del credito seleccionado,
                se realiza de esta forma ya que no se cuenta con un backend */
                var ownTable = data.data.filter(data => data.idCredit == idCredit)
                deferred.resolve(ownTable);
            }, function (error) {
                alert("error");
                deferred.reject(error);
            });
            return deferred.promise;
        }

        // Servicio que realiza el pago
        function doPayment(payment) {
            var deferred = $q.defer();
            $http({
                method: 'PUT',
                url: 'http://localhost:3000/tables/' + payment.id,
                data: payment
            })
                .then(function (data) {
                    deferred.resolve();
                }, function (error) {
                    alert("error");
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        // Servicio que crea el nuevo cliente
        function createNewCustomer(newCustomer) {
            var dataCustomer = {
                "id": newCustomer.id,
                "name": newCustomer.name
            }
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'http://localhost:3000/customers',
                data: dataCustomer
            })
                .then(function (data) {
                    deferred.resolve();
                }, function (error) {
                    alert("error");
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        // Servicio que crea el credito asignado del nuevo cliente
        function createNewCredit(newCustomer) {
            var dataCredit = {
                "id": newCustomer.id,
                "amount": newCustomer.amount,
                "idCustomer": newCustomer.id
            }
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'http://localhost:3000/credits',
                data: dataCredit
            })
                .then(function (data) {
                    deferred.resolve();
                }, function (error) {
                    alert("error");
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        // Servicio que crea las entradas de la tabla de amortización
        function createNewTable(newCustomer) {
            var dataTable = {
                "id": newCustomer.id,
                "idCredit": newCustomer.id,
                "amountPaid": 0,
                "paymentNumber": [
                    {
                        "no": 1,
                        "paymentStatus": false
                    },
                    {
                        "no": 2,
                        "paymentStatus": false
                    },
                    {
                        "no": 3,
                        "paymentStatus": false
                    },
                    {
                        "no": 4,
                        "paymentStatus": false
                    }
                ]
            }
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: 'http://localhost:3000/tables',
                data: dataTable
            })
                .then(function (data) {
                    deferred.resolve();
                }, function (error) {
                    alert("error");
                    deferred.reject(error);
                });
            return deferred.promise;
        }

        // Getters and Setters de Clientes, Creditos y Tablas de amortización seleccionadas para su reuso
        function setCustomer(data) {
            customer = {};
            customer = data;
        }

        function getCustomer() {
            return customer;
        }

        function setCredit(data) {
            credit = {};
            credit = data;
        }

        function getCredit() {
            return credit;
        }

        function setAmortizationTable(data) {
            table = {};
            table = data;
        }

        function getAmortizationTable() {
            return table;
        }

        return {
            getCustomers: getCustomers,
            getCredits: getCredits,
            getTable: getTable,
            setCustomer: setCustomer,
            getCustomer: getCustomer,
            setCredit: setCredit,
            getCredit: getCredit,
            setAmortizationTable: setAmortizationTable,
            getAmortizationTable: getAmortizationTable,
            doPayment: doPayment,
            createNewCustomer: createNewCustomer,
            createNewCredit: createNewCredit,
            createNewTable: createNewTable
        }
    });