'use strict';

angular.module('myApp.newCustomer', ['ngRoute'])
    .controller('newCustomerCtrl', ['$scope', 'customersService', '$location', '$q', function ($scope, customersService, $location, $q) {
        $scope.loading = false;
        $scope.newCustomer = {
            name: '',
            amount: '',
            id: 0
        }
        $scope.$onInit = function () {
            // Generamos un ID aleatorio para el nuevo cliente, se genera de esta forma pero en realizar es una tarea que debe asignar el Backend
            $scope.newCustomer.id = new Date().getUTCMilliseconds();
        };

        $scope.addNewCustomer = function () {
            $scope.loading = true;
            // Se realiza la creación del nuevo usuario dando de alta los campos en /customers, /credits, /tables
            $q.all([
                customersService.createNewCustomer($scope.newCustomer),
                customersService.createNewCredit($scope.newCustomer),
                customersService.createNewTable($scope.newCustomer)
            ]).then(values => {
                alert('Se ha dado de alta correctamente');
                $location.path('/home');
            }).catch(error =>{
                console.log(error);
                
            });
        }

        $scope.$onInit();
    }]);