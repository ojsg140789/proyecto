'use strict';

angular.module('myApp.credits', ['ngRoute'])
    .controller('creditsCtrl', ['$scope', '$routeParams', '$location', 'customersService',  function ($scope, $routeParams, $location, customersService) {
        $scope.loading = true;
        $scope.credits = [];
        $scope.customer = {};
        $scope.$onInit = function () {
            /* Recuperamos el cliente almacenado y consultamos sus creditos asignados */
            $scope.customer = customersService.getCustomer();
            customersService.getCredits($routeParams.idCustomer)
                .then(function (data) {
                    $scope.credits = data;
                    $scope.loading = false;
                })
                .catch(function (error) {
                    $scope.loading = false;
                    console.log('errctrl', error);
                });
        };

        $scope.goToTable = function(credit){
            // Almacenamos el credito seleccionado, así evitamos nuevamente su consumo y podemos reutilizar la información 
            customersService.setCredit(credit);
            $location.path('/table/'+credit.id);
        }
        $scope.$onInit();
    }]);