'use strict';

angular.module('myApp.home', ['ngRoute'])
    .controller('homeCtrl', ['$scope', 'customersService', '$location', function ($scope, customersService, $location) {
        $scope.loading = true;
        $scope.customers = []
        $scope.$onInit = function () {
            //Se obtiene el listado de los clientes
            customersService.getCustomers()
                .then(function (data) {
                    $scope.customers = data;
                    $scope.loading = false;
                })
                .catch(function (error) {
                    $scope.loading = false;
                    console.log('errctrl', error);
                });
        };

        $scope.goToCredits = function (customer){
            /*Se almacena el cliente seleccionado, así evitamos nuevamente el 
            consumo de este servicio y podemos reusar la información*/
            customersService.setCustomer(customer);
            $location.path('/credits/'+customer.id);
        }

        $scope.$onInit();
    }]);