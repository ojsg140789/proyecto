'use strict';

angular.module('myApp.table', ['ngRoute'])
    .controller('tableCtrl', ['$scope', '$routeParams', '$location', 'customersService', function ($scope, $routeParams, $location, customersService) {
        $scope.loading = true;
        $scope.table = {};
        $scope.credit = {};
        $scope.customer = {};
        $scope.payment = 0;
        $scope.btnPayment = false;
        $scope.$onInit = function () {
            // Recuperamos el cliente y el creditos seleccionados para la consulta de la tabla de amortización
            $scope.customer = customersService.getCustomer();
            $scope.credit = customersService.getCredit();
            customersService.getTable($routeParams.idCredit)
                .then(function (data) {
                    //Seleccionamos la posición 0 ya que customersService.getTable regresa un unico resultado en forma de arreglo
                    $scope.table = data[0];
                    /*Obtenemos el monto a pagar diviendo el monto entre el numero de pagos que se tienen que hacer, 
                        de esta forma no habría modificaciones en caso que el numero de pagos aumente o disminuya */
                    $scope.payment = $scope.credit.amount / $scope.table.paymentNumber.length;
                    // Verificamos si existe algun pago pendiente y de esta forma presentamos el botón 'Realizar Pago' si es el caso */
                    $scope.btnPayment = $scope.table.paymentNumber.every(element => element.paymentStatus == true);
                    $scope.loading = false;
                })
                .catch(function (error) {
                    $scope.loading = false;
                    console.log('errctrl', error);
                });
        };

        $scope.goToPayment = function(){
            // Almacenamos la información de la tabla de amortización para reutilizar su información y evitar nuevamente su consumo 
            customersService.setAmortizationTable($scope.table);
            $location.path('/payment');
        }
        $scope.$onInit();
    }]);