'use strict';

angular.module('myApp.payment', ['ngRoute'])
    .controller('paymentCtrl', ['$scope', '$location', 'customersService',  function ($scope, $location, customersService) {
        $scope.loading = false;
        $scope.table = {};
        $scope.credit = {};
        $scope.payment = {};
        $scope.paymentAmount = 0;
        $scope.$onInit = function () {
            // Recuperamos el credito y la tabla de amortización almacenada
            $scope.credit = customersService.getCredit();
            $scope.table = customersService.getAmortizationTable();
            /*Obtenemos el monto a pagar diviendo el monto entre el numero de pagos que se tienen que hacer, 
                        de esta forma no habría modificaciones en caso que el numero de pagos aumente o disminuya */
            $scope.paymentAmount = $scope.credit.amount / $scope.table.paymentNumber.length;
            // Obtenemos cual es el siguiente pago a realizar
            $scope.payment = $scope.table.paymentNumber.find( element => element.paymentStatus == false);
        };

        $scope.doPayment = function(){
            $scope.loading = true;
            /* Cambiamos el estatus del pago a realizar, 
            este paso lo hacemos de esta forma ya que no se cuenta con un real BackEnd pero es bien sabido que el estatus lo cambia el Backend*/
            $scope.payment.paymentStatus = true;
            /* Obtenemos el indes del pago a realizar para sustituirlo en la tabla de amortización,
            se realiza de esta forma ya que no contamos con un real Backend*/
            var index = $scope.table.paymentNumber.findIndex(value => value.no === $scope.payment.no);
            $scope.table.paymentNumber[index] = $scope.payment
            $scope.table.amountPaid += $scope.paymentAmount;
            customersService.doPayment($scope.table).then(
                function(data){
                    $scope.loading = false;
                    alert("Pago realizado exitosamente");
                    $location.path('/table/'+$scope.table.id);
                },
                function(error){
                    // Devolvemos el estatus del pago a realizar en caso de que ocurra un falló en el guardado
                    $scope.payment.paymentStatus = false;
                    $scope.loading = false;
                    console.log(error);
                    
                }
            );
        }
        $scope.$onInit();
    }]);