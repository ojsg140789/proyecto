'use strict';

// Declare app level module which depends on views, and core components
angular.module('myApp', [
  'ngRoute',
  'myApp.home',
  'myApp.credits',
  'myApp.table',
  'myApp.payment',
  'myApp.version',
  'myApp.newCustomer',
  'myApp.customersService'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
  .when('/home', {
    templateUrl: 'components/home/home.html',
    controller: 'homeCtrl'
  })
  .when('/credits/:idCustomer', {
    templateUrl: 'components/credits/credits.html',
    controller: 'creditsCtrl'
  })
  .when('/table/:idCredit', {
    templateUrl: 'components/table/table.html',
    controller: 'tableCtrl'
  })
  .when('/payment', {
    templateUrl: 'components/payment/payment.html',
    controller: 'paymentCtrl'
  })
  .when('/newCustomer', {
    templateUrl: 'components/newCustomer/newCustomer.html',
    controller: 'newCustomerCtrl'
  })
  .otherwise({redirectTo: '/home'});
}]);
