# Version
AngularJS: 1.7.5
Node: 12.16.2
NPM: 6.14.4

# Clone and Install

Ejecutar el comando 'clone' de git del depositorio

```
git clone https://gitlab.com/ojsg140789/proyecto
```
Con la linea de comando accedes a la carpeta 'proyecto'

Ejecutas el comando para instalar las dependencias:

```
npm install
```

### Run the Application
Se tiene preconfigurado el proyecto en un simple server web, la simple forma de iniciarlo es con el comando:

```
npm start
```

Ahora la aplicación se podrá observar en la siguiente ruta:

[`localhost:8000`]

### Fake Server
¡IMPORTANTE!

Para que la aplicación funcione correctamente es fundamental clonar el repositorio del fake server de la siguiente liga:
https://gitlab.com/ojsg140789/server-client

Ejecutamos los comandos 

```
git clone https://gitlab.com/ojsg140789/server-client
```
Con la linea de comando accedes a la carpeta 'server-client'

Ejecutas el comando para instalar las dependencias:

```
npm install
```

Para iniciar json-server simplemente ejecutamos el comando:

```
npm start
```

Ahora podemos ver json-server corriendo en :

[`localhost:3000`]


## Directory Layout

```
app/                  --> all of the source files for the application
  app.css               --> default stylesheet
  core/                 --> all app specific modules
    version/              --> version related components
      version.js                 --> version module declaration and basic "version" value service
      version_test.js            --> "version" value service tests
      version-directive.js       --> custom directive that returns the current app version
      version-directive_test.js  --> version directive tests
      interpolate-filter.js      --> custom interpolation filter
      interpolate-filter_test.js --> interpolate filter tests
  components/         --> Contain all components
    credits/             --> the credits view template and logic
      credits.html            --> the partial template
      creditsController.js    --> the controller logic
    home/                --> the home view template and logic
      home.html             --> the partial template
      homeController.js     --> the controller logic
    newCustomer/         --> the newCustomer view template and logic
      newCustomer.html          --> the partial template
      newCustomerController.js  --> the controller logic
    payment/             --> the payment view template and logic
      payment.html            --> the partial template
      paymentController.js    --> the controller logic
    table/               --> the table view template and logic
      table.html            --> the partial template
      tableController.js    --> the controller logic
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
karma.conf.js         --> config file for running unit tests with Karma
package.json          --> Node.js specific metadata, including development tools dependencies
package-lock.json     --> Npm specific metadata, including versions of installed development tools dependencies
